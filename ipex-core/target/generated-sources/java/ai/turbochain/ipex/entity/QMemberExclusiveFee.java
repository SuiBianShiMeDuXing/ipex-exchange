package ai.turbochain.ipex.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMemberExclusiveFee is a Querydsl query type for MemberExclusiveFee
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMemberExclusiveFee extends EntityPathBase<MemberExclusiveFee> {

    private static final long serialVersionUID = 1433184414L;

    public static final QMemberExclusiveFee memberExclusiveFee = new QMemberExclusiveFee("memberExclusiveFee");

    public final DateTimePath<java.util.Date> createTime = createDateTime("createTime", java.util.Date.class);

    public final NumberPath<java.math.BigDecimal> fee = createNumber("fee", java.math.BigDecimal.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> memberId = createNumber("memberId", Long.class);

    public final StringPath symbol = createString("symbol");

    public final NumberPath<Integer> type = createNumber("type", Integer.class);

    public QMemberExclusiveFee(String variable) {
        super(MemberExclusiveFee.class, forVariable(variable));
    }

    public QMemberExclusiveFee(Path<? extends MemberExclusiveFee> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMemberExclusiveFee(PathMetadata metadata) {
        super(MemberExclusiveFee.class, metadata);
    }

}

